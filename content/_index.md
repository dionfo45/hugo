## Martin Luther King Jr

Martin Luther King Jr., plus couramment appelé Martin Luther King, né à Atlanta, en Géorgie, le 15 janvier 1929, et mort assassiné le 4 avril 1968 à Memphis, dans le Tennessee, est un pasteur, baptiste et militant non-violent afro-américain pour le mouvement des droits civiques des noirs américains aux États-Unis, fervent militant pour la paix et contre la pauvreté.

Il organise et dirige des actions telles que le boycott des bus de Montgomery pour défendre le droit de vote, la déségrégation et l'emploi des minorités ethniques.

Il prononce un discours célèbre le 28 août 1963 devant le Lincoln Memorial à Washington, D.C. durant la marche pour l'emploi et la liberté : il s'intitule « I have a dream ». Ce discours est soutenu par John Fitzgerald Kennedy dans la lutte contre la ségrégation raciale aux États-Unis ; le président Lyndon B. Johnson par une plaidoirie infatigable auprès des membres du Congrès arrivera à faire voter différentes lois fédérales comme le Civil Rights Act de 1964, le Voting Rights Act de 1965 et le Civil Rights Act de 1968 mettant juridiquement fin à toutes les formes de ségrégation raciale sur l'ensemble des États-Unis.

Martin Luther King devient le plus jeune lauréat du prix Nobel de la paix en 1964 pour sa lutte non-violente contre la ségrégation raciale et pour la paix. Il commence alors une campagne contre la guerre du Viêt Nam et la pauvreté, qui prend fin en 1968 avec son assassinat officiellement attribué à James Earl Ray, dont la culpabilité et la participation à un complot sont toujours débattues.

Il se voit décerner à titre posthume la médaille présidentielle de la Liberté par Jimmy Carter en 1977, le prix des droits de l'homme des Nations unies en 1978, la médaille d'or du Congrès en 2004, et est considéré comme l'un des plus grands orateurs américains1.

Depuis 1986, le Martin Luther King Day est un jour férié aux États-Unis. Deux centres Martin Luther King pour l'action non-violente existent, l'un en Suisse à Lausanne et l'autre à Atlanta. De nombreux autres monuments (musées, écoles) sont répertoriés sous le nom de Martin Luther King partout dans le monde.
